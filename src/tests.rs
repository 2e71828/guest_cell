use proptest::prelude::*;
use super::*;
use std::cell::{RefCell, Ref};

proptest! {
    #[test]
    fn test_guest_cell(vals in prop::collection::vec(1u32..1000, 1..100)) {
        let cell: GuestCell<u32> = GuestCell::default();
        {
            let guests: Vec<RefCell<Guest<'_>>> = vals.iter().map(|_| Default::default()).collect();
            let mut init_refs: Vec<Ref<'_, u32>> = vec![];

            for (i,&v) in vals.iter().enumerate() {
                guests[i].borrow_mut().set(&cell, Box::new(v));
                init_refs.push(
                    Ref::map(guests[i].borrow(),
                        |guest| guest.get(&cell).unwrap()
                    )
                );
                assert_eq!(*init_refs[i], v);
            }

            for (i, guest) in guests.iter().enumerate() {
                let guest = guest.borrow();
                let fin_ref = guest.get(&cell).unwrap();
                assert_eq!(*fin_ref, vals[i]);
                assert_eq!((&* init_refs[i]) as *const u32, fin_ref as *const u32);
            }
        }
        assert_eq!(cell.len(), 0);
    }
}
