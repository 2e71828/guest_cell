//! A container which provides typed scratch storage for external owners.

// Copyright 2023 Eric Michael Sumner
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::collections::{HashMap,HashSet};
use std::hash::{Hash,Hasher};
use std::borrow::Borrow;

use parking_lot::Mutex;

#[cfg(test)] mod tests;
pub mod id;
use id::{Id,UniqueId};

/// A typed slot for `Guest`s to store private values.
/// Stores multiple values, one per guest.
#[derive(Debug)]
pub struct GuestCell<T:?Sized> {
    map: Mutex<HashMap<Id, *mut T>>,
    id: UniqueId
}

impl<T:?Sized> Default for GuestCell<T> {
    fn default()->Self {
        GuestCell {
            map: Mutex::new(HashMap::new()),
            id: UniqueId::default()
        }
    }
}

impl<T:?Sized> GuestCell<T> {
    pub fn new()->Self { Self::default() }
}

// GuestCell acts much likd Mutex<T>
unsafe impl<T:?Sized + Send> Send for GuestCell<T> {}
unsafe impl<T:?Sized + Send> Sync for GuestCell<T> {}

/// Accessor for a `GuestCell`.
/// Safety: `borrow` must always return the same `GuestCell`
pub unsafe trait IndirectGuestCell: Borrow<GuestCell<Self::Inner>> {
    type Inner: ?Sized;
}

unsafe impl<T:?Sized> IndirectGuestCell for &'_ GuestCell<T> { type Inner = T; }
unsafe impl<T:?Sized> IndirectGuestCell for std::sync::Arc<GuestCell<T>> { type Inner = T; }
unsafe impl<T:?Sized> IndirectGuestCell for std::rc::Rc<GuestCell<T>> { type Inner = T; }

trait ErasedGuestCell {
    /// Drop the stored value placed here by `guest`.
    fn guest_drop(&self, guest: &mut UniqueId);

    /// Returns an identifier that represents this cell
    fn id(&self)->&UniqueId;

    /// Returns the number of guests that have stored value in this cell.
    ///
    /// Note: This is provided for diagnostic use; beware TOCTOU issues if
    /// using this to make actual decisions.
    fn len(&self)->usize;
}

impl<T:?Sized> ErasedGuestCell for GuestCell<T> {
    fn guest_drop(&self, guest_id: &mut UniqueId) {
        let ptr = self.map.lock().remove(&guest_id).unwrap();

        // Safety: This pointer was generated via `Box::into_raw`
        //         and is not currently aliased, due to the &mut UniqueId
        let _ = unsafe { Box::from_raw(ptr) };
    }

    fn id(&self)->&UniqueId { &self.id }

    fn len(&self)->usize { self.map.lock().len() }
}

impl<Ptr:IndirectGuestCell> ErasedGuestCell for Ptr {
    fn guest_drop(&self, guest: &mut UniqueId) {
        self.borrow().guest_drop(guest)
    }

    fn id(&self)->&UniqueId {
        self.borrow().id()
    }

    fn len(&self)->usize { self.borrow().len() }
}

impl PartialEq for dyn ErasedGuestCell + '_ {
    fn eq(&self, rhs: &dyn ErasedGuestCell)->bool {
        **self.id() == **rhs.id()
    }
}

impl Eq for dyn ErasedGuestCell + '_ {}

impl Hash for dyn ErasedGuestCell + '_ {
    fn hash<H:Hasher>(&self, h:&mut H) {
        self.id().hash(h);
    }
}

/// An owner for private values stored in various `GuestCell`s.
#[derive(Default)]
pub struct Guest<'a> {
    id: UniqueId,
    cells: HashSet<Box<dyn ErasedGuestCell + 'a>>,
}

impl Drop for Guest<'_> {
    fn drop(&mut self) {
        for map in &self.cells {
            map.guest_drop(&mut self.id);
        }
    }
}

impl std::fmt::Debug for Guest<'_> {
    fn fmt(&self, f:&mut std::fmt::Formatter<'_>)->Result<(), std::fmt::Error> {
        f.debug_struct("Guest")
            .field("id", &self.id)
            .field("cells", &self.cells.iter().map(|x| x.id()).collect::<Vec<_>>())
            .finish()
    }
}

impl<'a> Guest<'a> {
    /// Alias for Default::default()
    pub fn new()->Self { Self::default() }

    pub fn get<T:?Sized>(&self, cell: impl 'a + IndirectGuestCell<Inner=T>)->Option<&T> {
        let guard = cell.borrow().map.lock();
        let ptr:*mut T = *guard.get(&self.id)?;

        // Safety: Access to this box is mediated through self.id 
        Some(unsafe { &*ptr })
    }

    pub fn get_mut<T:?Sized>(&mut self, cell: impl 'a + IndirectGuestCell<Inner=T>)->Option<&mut T> {
        let guard = cell.borrow().map.lock();
        let ptr:*mut T = *guard.get(&self.id)?;

        // Safety: Access to this box is mediated through self.id 
        Some(unsafe { &mut *ptr })
    }

    pub fn get_or_init_mut<T:?Sized>(&mut self,
                                 cell: impl 'a + IndirectGuestCell<Inner=T>,
                                 init: impl FnOnce()->Box<T>)->&mut T {
        let ptr:*mut T = {
            let mut guard = cell.borrow().map.lock();
            *guard.entry(*self.id).or_insert_with(|| Box::into_raw(init()))
        };

        self.cells.insert(Box::new(cell));
        // Safety: Access to this box is mediated through self.id 
        unsafe { &mut *ptr }
    }

    pub fn set<T:?Sized>(&mut self, cell: impl 'a + IndirectGuestCell<Inner=T>, val:Box<T>)->Option<Box<T>> {
        let result = cell.borrow().map.lock().insert(*self.id, Box::into_raw(val));
        self.cells.insert(Box::new(cell));

        // Safety: This value was generated via `Box::into_raw`, and has no live references
        Some(unsafe { Box::from_raw(result?) })
    }

    pub fn take<T:?Sized>(&mut self, cell: impl 'a + IndirectGuestCell<Inner=T>)->Option<Box<T>> {
        let result = cell.borrow().map.lock().remove(&self.id);
        self.cells.remove(&cell as &dyn ErasedGuestCell);

        // Safety: This value was generated via `Box::into_raw`, and has no live references
        Some(unsafe { Box::from_raw(result?) })
    }
}
